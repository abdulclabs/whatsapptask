//
//  GlobalData.swift
//  What6sAppTask
//
//  Created by Click Labs on 3/19/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import Foundation

// constant used in alert...
let title = "No WhatsApp Installed!!"
let message = " Please Install WhatsApp..."
let okText = "Ok"